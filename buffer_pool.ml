module BA = Bigarray
module BA1 = BA.Array1

type buffer = Shell_probe.Tracing.buffer

type t = { dim : int; queue : buffer Queue.t }

let create dim = { dim; queue = Queue.create () }

let uninitialized_buffer (len : int) =
  if len < 0 then invalid_arg "uninitialized_buffer" ;
  BA1.create BA.Int BA.c_layout len

let get pool =
  match Queue.take_opt pool.queue with
  | None -> uninitialized_buffer pool.dim
  | Some buf -> buf

let free pool buf = Queue.add buf pool.queue

let size { queue; _ } = Queue.length queue
