(** The [Tracing] module allows to record with low latency integer-encoded
    events. Moreover, the [Tracing] module allows to record periodically
    (every [n] events, for a configurable [n]) some {e metric} specified by
    the user.

    A typical operation of this module goes as follows:
    - create a tracing [state]
    - Call [set_metric_measurement_frequency] to a suitable value
    - Call [set_metric_callback] to a suitable metric
    - Call [set_snapshot_callback] to a suitable value, possibly using
      the helpers in [Packet_iterator].
    - Perform measurements using [record]
    - Once finished, possibly perform a call to [flush] to trigger
      snapshotting remaining packets and handling trailing events.

    Lexicon: we call a {e packet} a bunch of events sandwiched between
    two calls to the [metric_callback]. The library holds such packets
    in internal buffers. When no more packet can be stored, a call to
    [snapshot_callback] is performed for the user to do something with
    these packets, after which the internal buffers are reused for
    further metric and event collection.

 *)

(** The type of buffers, used to record events and metrics. *)
type buffer = (int, Bigarray.int_elt, Bigarray.c_layout) Bigarray.Array1.t

(** The type of tracing states. *)
type state

(** The type of user-provided callbacks, called to record metrics. *)
type metric_callback = unit -> int

(** The type of user-provided callbacks, called when event and metric
    data needs to be processed. *)
type snapshot_callback =
  events:buffer -> metrics_count:int -> metrics:buffer -> stride:int -> unit

(** [create ~buffer_length] creates a fresh tracing state. This states stores
    events and metrics collected using [record] and the user-specified
    [metric]. When the number of recorded events or recorded metrics reaches
    [buffer_length], a call to the currently set [snapshot_callback] is
    performed with the current contents of the buffers.

    @raise Invalid_argument if [buffer_length <= 1]
 *)
val create : buffer_length:int -> state

(** [buffer_length state] is the length of the internal buffers as set at
    state creation time. *)
val buffer_length : state -> int

(** [set_metric_callback] sets the currently used metric callback.
    Changing the metric callback can be performed anytime. *)
val set_metric_callback : state -> metric_callback -> unit

(** [set_metric_measurement_frequency state ~delta_events] updates the
    metric recording period so that

    Calling [set_metric_measurement_frequency] will flush the buffers:
    all currently held events are lost.

    Calling [set_metric_measurement_frequency] will also trigger a
    cal to the currently registered [metric_callback] on the next
    all to [record].

    @raise Invalid_argument if [delta_events <= 0 || delta_events >= buffer_length] *)
val set_metric_measurement_frequency : state -> delta_events:int -> unit

(** [set_snapshot_callback] sets the currently used snapshot callback.
    Changing the snapshot callback can be performed anytime. *)
val set_snapshot_callback : state -> snapshot_callback -> unit

(** [record s ev] records the event [ev]. This can trigger calls to
    the currently set [metric_callback] or [snapshot_callback]. *)
val record : state -> int -> unit

(** [flush state ~trailing_callback] calls the [snapshot_callback]
    on the unprocessed packets currently held in the internal state.
    There might be some trailing events (ie a partial packet):
    if that's the case, [trailing_callback] is called with the
    [events] buffer and the {b inclusive} range of indices in
    which the trailing events are. *)
val flush :
  state ->
  trailing_callback:
    (events:buffer ->
    first_trailing_index:int ->
    last_trailing_index:int ->
    unit) ->
  unit

(** The [Packet_iterator] module can be used to perform incremental processing
    of the packets returned to the user through the [snapshot_callback]. *)
module Packet_iterator : sig
  (** The type of the internal state of the packet iterator. *)
  type state

  (** [init ~metrics_count ~stride] returns [Some state] if there
      are some packets to process, [None] otherwise.
      The number of packets is [metrics_count - 1]. *)
  val init : metrics_count:int -> stride:int -> state option

  (** [next state ~events ~metrics ~on_event ~on_new_packet] allows to
      process packets incrementally. [events] and [metrics] are typically
      given though the [snapshot_callback] parameters. [on_event] is called on
      each event, while [on_new_packet] is called when starting processing a
      new packet. *)
  val next :
    state ->
    events:buffer ->
    metrics:buffer ->
    on_event:(int -> unit) ->
    on_new_packet:(metric1:int -> metric2:int -> unit) ->
    bool
end
