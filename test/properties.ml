module Tracing = Shell_probe.Tracing

let make_metric () =
  let x = ref 0 in
  ( x,
    fun () ->
      let v = !x in
      incr x ;
      v )

type data = Packet of int * int * int list ref

let make_snapshot_callback () =
  let packets = ref [] in
  let callback ~events ~metrics_count ~metrics ~stride =
    match Shell_probe.Tracing.Packet_iterator.init ~metrics_count ~stride with
    | None ->
        (* empty chunk: no packets to process *)
        ()
    | Some iterator ->
        let on_event ev =
          match !packets with
          | [] -> assert false
          | Packet (_m1, _m2, events) :: _ -> events := ev :: !events
        in
        let on_new_packet ~metric1 ~metric2 =
          match !packets with
          | [] -> packets := [Packet (metric1, metric2, ref [])]
          | _ -> packets := Packet (metric1, metric2, ref []) :: !packets
        in
        while
          Shell_probe.Tracing.Packet_iterator.next
            iterator
            ~events
            ~metrics
            ~on_event
            ~on_new_packet
        do
          ()
        done
  in
  (packets, callback)

type parameters =
  { buffer_length : int; delta_events : int; total_events : int }

let pp_parameters { buffer_length; delta_events; total_events } =
  Format.asprintf
    "@[{ buffer_length = %d; delta_events = %d; total_events = %d }@]"
    buffer_length
    delta_events
    total_events

let parameters_gen =
  QCheck.Gen.(
    2 -- 1024 >>= fun buffer_length ->
    1 -- (buffer_length - 1) >>= fun delta_events ->
    1 -- 2048 >>= fun total_events ->
    return { buffer_length; delta_events; total_events })

(* Checks that [list] contains all the elements in the inclusive interval (low, hi). *)
let check_contains_all (list : int list) (low, hi) =
  let rec loop list low =
    match list with
    | [] -> if low = hi + 1 then `Ok else `Overflow low
    | elt :: tl -> if low = elt then loop tl (low + 1) else `Mismatch (low, elt)
  in
  loop list low

let check_contains_prefix (list : int list) (low, hi) =
  let rec loop list low =
    match list with
    | [] -> if low <= hi + 1 then `Ok else `Overflow low
    | elt :: tl -> if low = elt then loop tl (low + 1) else `Mismatch (low, elt)
  in
  loop list low

let pp_int_list fmtr l =
  Format.pp_print_list
    ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",@;")
    Format.pp_print_int
    fmtr
    l

let bad = { buffer_length = 316; delta_events = 146; total_events = 1368 }

let recorded_events_prefix_test { buffer_length; delta_events; total_events } =
  let state = Tracing.create ~buffer_length in
  let (_generated, metric) = make_metric () in
  let (packets, snapshot) = make_snapshot_callback () in
  Tracing.set_metric_callback state metric ;
  Tracing.set_metric_measurement_frequency state ~delta_events ;
  Tracing.set_snapshot_callback state snapshot ;
  for i = 1 to total_events do
    Tracing.record state i
  done ;
  let packets = List.rev !packets in
  let all_events =
    List.concat
      (List.map
         (function Packet (_m1, _m2, events) -> List.rev !events)
         packets)
  in
  match check_contains_prefix all_events (1, total_events) with
  | `Ok -> true
  | `Overflow bound ->
      Format.printf "emitted:@." ;
      Format.printf "%a@." pp_int_list all_events ;
      Format.printf "overflow: %d@." bound ;
      false
  | `Mismatch (low, elt) ->
      Format.printf "emitted:@." ;
      Format.printf "%a@." pp_int_list all_events ;
      Format.printf "mismatch: low = %d, elt = %d@." low elt ;
      false

let check_recorded_events_prefix =
  QCheck.Test.make
    ~count:10_000
    ~name:"captured events are a prefix of emitted events"
    (QCheck.make ~print:pp_parameters parameters_gen)
    recorded_events_prefix_test

let filter_dup l =
  let rec loop (l : int list) prev =
    match l with
    | [] -> []
    | hd :: tl -> if prev = hd then loop tl prev else hd :: loop tl hd
  in
  match l with [] | [_] -> l | hd :: tl -> hd :: loop tl hd

let recorded_metrics_prefix_test { buffer_length; delta_events; total_events } =
  let state = Tracing.create ~buffer_length in
  let (generated, metric) = make_metric () in
  let (packets, snapshot) = make_snapshot_callback () in
  Tracing.set_metric_callback state metric ;
  Tracing.set_metric_measurement_frequency state ~delta_events ;
  Tracing.set_snapshot_callback state snapshot ;
  for i = 1 to total_events do
    Tracing.record state i
  done ;
  let packets = List.rev !packets in
  let all_metrics =
    List.concat
      (List.map (function Packet (m1, m2, _events) -> [m1; m2]) packets)
  in
  match check_contains_prefix all_metrics (0, !generated - 1) with
  | `Ok -> true
  | `Overflow bound ->
      Format.printf "emitted:@." ;
      Format.printf "%a@." pp_int_list all_metrics ;
      Format.printf "overflow: %d@." bound ;
      false
  | `Mismatch (low, elt) ->
      Format.printf "emitted:@." ;
      Format.printf "%a@." pp_int_list all_metrics ;
      Format.printf "mismatch: low = %d, elt = %d@." low elt ;
      false

let () = QCheck.Test.check_exn check_recorded_events_prefix

let check_recorded_events_with_trailing_complete =
  QCheck.Test.make
    ~count:100_000
    ~name:"captured events + trailing are exactly all emitted events"
    (QCheck.make ~print:pp_parameters parameters_gen)
    (fun { buffer_length; delta_events; total_events } ->
      let state = Tracing.create ~buffer_length in
      let (_generated, metric) = make_metric () in
      let (packets, snapshot) = make_snapshot_callback () in
      Tracing.set_metric_callback state metric ;
      Tracing.set_metric_measurement_frequency state ~delta_events ;
      Tracing.set_snapshot_callback state snapshot ;
      for i = 1 to total_events do
        Tracing.record state i
      done ;
      let trailing = ref [] in
      Tracing.flush
        state
        ~trailing_callback:(fun
                             ~events
                             ~first_trailing_index
                             ~last_trailing_index
                           ->
          for i = first_trailing_index to last_trailing_index do
            trailing := events.{i} :: !trailing
          done) ;

      let packets = List.rev !packets in
      let all_non_trailing_events =
        List.concat
          (List.map
             (function Packet (_m1, _m2, events) -> List.rev !events)
             packets)
      in
      let trailing_events = List.rev !trailing in
      let all_events = all_non_trailing_events @ trailing_events in
      match check_contains_all all_events (1, total_events) with
      | `Ok -> true
      | `Overflow bound ->
          Format.printf "overflow: %d@." bound ;
          false
      | `Mismatch (low, elt) ->
          Format.printf "mismatch: low = %d, elt = %d@." low elt ;
          false)

let () = QCheck.Test.check_exn check_recorded_events_with_trailing_complete
