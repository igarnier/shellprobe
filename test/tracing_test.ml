module BA = Bigarray
module BA1 = BA.Array1
open Shell_probe

type t = Event of int | Metrics of int * int

let () =
  let state = Tracing.create ~buffer_length:(32768 * 8) in
  Tracing.set_metric_callback state (fun () ->
      int_of_float (1000.0 *. Unix.gettimeofday ())) ;
  Tracing.set_metric_measurement_frequency state ~delta_events:128 ;
  let shared = ref None in
  Process.start
    state
    ~debug_interval:0
    ~init:(fun () ->
      let file =
        Filename.temp_file ~temp_dir:"/home/ilias/tmp" "shellprobe_" ".dat"
      in
      let oc = open_out_gen [Open_wronly; Open_creat; Open_trunc] 0o640 file in
      shared := Some oc)
    ~on_event:(fun i -> ignore i)
      (* let oc = Option.get !shared in
       * Marshal.to_channel oc (Event i) []) *)
    ~on_new_packet:(fun ~metric1 ~metric2 ->
      let oc = Option.get !shared in
      Marshal.to_channel oc (Metrics (metric1, metric2)) [])
      (* (fun ~metrics_count ~events_count:_ ~stride ~events ~metrics ->
       * let oc = Option.get !shared in
       * let chunks = Tracing.chunks ~metrics ~metrics_count ~stride in
       * Array.iter
       *   (fun Tracing.{ event_start; event_stop; metric_before; metric_after } ->
       *     let sub_events =
       *       BA1.sub events event_start (event_stop - event_start + 1)
       *     in
       *     Marshal.to_channel oc (sub_events, metric_before, metric_after) [])
       *   chunks) *)
    ~cleanup:(fun () -> Option.iter close_out !shared) ;
  let bef = Unix.gettimeofday () in
  let dummy_acc = ref 0 in
  for _ = 1 to 500_000 do
    for i = 1 to 1024 do
      dummy_acc := !dummy_acc + i ;
      Tracing.record state !dummy_acc
    done
  done ;
  let aft = Unix.gettimeofday () in
  Tracing.flush
    state
    ~trailing_callback:(fun
                         ~events:_
                         ~first_trailing_index:_
                         ~last_trailing_index:_
                       -> ()) ;
  let processing_ms = 1_000.0 *. (aft -. bef) in
  Format.printf "processing time (ms): %f@." processing_ms
