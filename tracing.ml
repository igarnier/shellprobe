module BA = Bigarray
module BA1 = BA.Array1

type buffer = (int, Bigarray.int_elt, Bigarray.c_layout) BA1.t

type counters =
  { mutable events_recorded : int;
    mutable events_since_last_metric : int;
    mutable metrics_recorded : int
  }

type metric_callback = unit -> int

type snapshot_callback =
  events:buffer -> metrics_count:int -> metrics:buffer -> stride:int -> unit

type state =
  { buffer_length : int;
    counters : counters;
    events : buffer;
    metrics : buffer;
    mutable metric_sampling_interval : int;
    mutable metric_callback : metric_callback;
    mutable snapshot_callback : snapshot_callback
  }

let reset_counters state =
  let c = state.counters in
  c.events_recorded <- 0 ;
  c.events_since_last_metric <- 0 ;
  c.metrics_recorded <- 0

let set_metric_measurement_frequency state ~delta_events =
  reset_counters state ;
  if delta_events <= 0 || delta_events >= state.buffer_length then
    invalid_arg "set_metric_measurement_frequency" ;
  state.metric_sampling_interval <- delta_events ;
  (* dev note: the next line sets up the state so that on the next record,
     a call to the metric is guaranteed *)
  state.counters.events_since_last_metric <- delta_events

let create ~buffer_length =
  if buffer_length <= 1 then invalid_arg "create" ;
  let default_interval = 10 in
  let metric_sampling_interval = 1 + (buffer_length / default_interval) in
  let state =
    { buffer_length;
      counters =
        { events_recorded = 0;
          events_since_last_metric = 0;
          metrics_recorded = 0
        };
      (* dev note: having buffers of constant length over a run allows
         to easily recycle these buffers; we rely on this invariant
         in Buffer_pool *)
      events = BA1.create BA.Int BA.c_layout buffer_length;
      metrics = BA1.create BA.Int BA.c_layout buffer_length;
      metric_sampling_interval;
      metric_callback = (fun _ -> 0);
      snapshot_callback =
        (fun ~events:_ ~metrics_count:_ ~metrics:_ ~stride:_ -> ())
    }
  in
  set_metric_measurement_frequency state ~delta_events:metric_sampling_interval ;
  state

let buffer_length state = state.buffer_length

let set_metric_callback state f = state.metric_callback <- f

let set_snapshot_callback state f = state.snapshot_callback <- f

let record state ev =
  let counters = state.counters in
  let events = state.events in
  let current = counters.events_recorded in
  let stride = state.metric_sampling_interval in
  events.{current} <- ev ;
  counters.events_recorded <- current + 1 ;
  counters.events_since_last_metric <- counters.events_since_last_metric + 1 ;
  if counters.events_since_last_metric >= stride then (
    let metrics = state.metrics in
    let current_metric = counters.metrics_recorded in
    metrics.{current_metric} <- state.metric_callback () ;
    counters.metrics_recorded <- current_metric + 1 ;
    counters.events_since_last_metric <- 0 ;
    (* Is this the last metric we can record before the event buffer
       becomes full? If so, flush. *)
    if counters.events_recorded + stride >= state.buffer_length + 1 then (
      (* events.(current) is not a valid sample yet, save it
         for the next run. *)
      state.snapshot_callback
        ~events
        ~metrics_count:counters.metrics_recorded
        ~metrics
        ~stride ;
      counters.events_recorded <- 1 ;
      counters.metrics_recorded <- 1 ;
      metrics.{0} <- metrics.{current_metric} ;
      events.{0} <- events.{current})
    else ())
  else ()

(* These are the events that are in no packet *)
let trailing_events state callback =
  let packets_count = state.counters.metrics_recorded - 1 in
  if packets_count < 0 then ()
  else if packets_count = 0 then
    let first_event_index = 0 in
    let last_event_index = state.counters.events_recorded - 1 in
    callback
      ~events:state.events
      ~first_trailing_index:first_event_index
      ~last_trailing_index:last_event_index
  else
    let last_packet_index = packets_count - 1 in
    let stride = state.metric_sampling_interval in
    let first_event_index = (last_packet_index + 1) * stride in
    let last_event_index = state.counters.events_recorded - 1 in
    assert (first_event_index <= last_event_index) ;
    callback
      ~events:state.events
      ~first_trailing_index:first_event_index
      ~last_trailing_index:last_event_index

(* Flush can happen in a middle of a run.
   We have to discard the trailing events, ie the events not bracketed
   between two calls to the metric. *)
let flush state ~trailing_callback =
  if state.counters.metrics_recorded = 0 then
    (* This can only happen if [record] was never called. There can be
       no trailing event. *)
    ()
  else if state.counters.metrics_recorded = 1 then
    (* After the first call to [record], [metrics_recorded] is always > 0.
       If [metrics_recorded] = 1, it means that the no full packet was
       produced: there might be some trailing events. *)
    trailing_events state trailing_callback
  else (
    (* In order to not discard the trailing events here, we need to have
       a dedicated 'flush' callback for the trailing events *)
    state.snapshot_callback
      ~events:state.events
      ~metrics_count:state.counters.metrics_recorded
      ~metrics:state.metrics
      ~stride:state.metric_sampling_interval ;
    trailing_events state trailing_callback) ;
  reset_counters state

module Packet_iterator = struct
  type state =
    { mutable packet_index : int;
      mutable event_index : int;
      mutable last_event_index : int;
      stride : int;
      last_packet_index : int
    }

  let next state ~(events : buffer) ~(metrics : buffer) ~on_event ~on_new_packet
      =
    if state.event_index <= state.last_event_index then (
      let current = state.event_index in
      state.event_index <- state.event_index + 1 ;
      on_event events.{current} ;
      true)
    else if state.packet_index < state.last_packet_index then (
      state.packet_index <- state.packet_index + 1 ;
      let packet_index = state.packet_index in
      state.last_event_index <- ((packet_index + 1) * state.stride) - 1 ;
      on_new_packet
        ~metric1:metrics.{packet_index}
        ~metric2:metrics.{packet_index + 1} ;
      true)
    else false

  let init ~metrics_count ~stride =
    let packets_count = metrics_count - 1 in
    if packets_count <= 0 then None
    else
      Some
        { packet_index = -1;
          event_index = 0;
          last_event_index = -1;
          stride;
          last_packet_index = packets_count - 1
        }
end
