# shellprobe

`shellprobe` allows to perform low-latency tracing of OCaml programs.

TL;DR:
- choose the tracing parameters wisely so as not to impact the behaviour of the
  traced program.
- benchmark the overhead of tracing as a function of these parameters

- Q. What kind of event can be recorded?
- A. Currently, `shellprobe` events and metrics must be encodable as OCaml integers.

- Q. What is the tracing API offered by `shellprobe`?
- A. Please have a look at `tracing.mli`. In a nutshell:
     1) The user must instantiate an object holding the internal event and metric buffers,
        specifying their size.
     2) The user must also specify a *metric* callback, which is going to be called
        periodically every `n` events.
     3) Finally, the user must specify what is to be done when the internal buffers
        are full by giving a *snapshot* callback.
     4) When this initialization is performed, the user needs simply to call the
        `Tracing.record` function.

- Q. What is meant by 'low-latency'?
- A. Tracing an event should incur a negligible overhead. It amounts for most calls to
     writing an integer in a buffer and performing a test.
     Periodically, an additional call to the metric callback is performed.
     Finally, when the buffers are full, an additional call to the snapshot callback is performed.

     The metric and snapshot callbacks are user-specified, so the latency induced by
     those should be carefully assessed by he user of `shellprobe`.

- Q. What is the output format of the traces?
- A. The snapshot callback specifies that the recorded data is formatted as 'packets'
     of events sandwiched by the outcome of calls to the metric callback. It is up
     to the user to decide on a storage format for the traces.

- Q. What is the best way to process events when the snapshot callback is called?
- A. If event processing is lightweight and the tracing frequency is not too high,
     one can process the events directly in the process being instrumented.
     However, for high-frequency tracing, it is probably wiser to offload the
     processing (including writing to disk) to another process.
     The `Process` module will do that for you.
