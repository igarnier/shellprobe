module BA = Bigarray
module BA1 = BA.Array1
module BA2 = BA.Array2

type buffer = Shell_probe.Tracing.buffer

(* ------------------------------------------------------------------------- *)
(* Unix helpers *)

let read_byte ~__LOC__ buf fd =
  match Unix.read fd buf 0 1 with
  | exception _ -> Format.ksprintf failwith "%s: read failed" __LOC__
  | 1 -> ()
  | 0 -> raise End_of_file
  | _ -> Format.ksprintf failwith "%s: read failed" __LOC__

let write_byte ~__LOC__ buf fd =
  match Unix.write fd buf 0 1 with
  | exception _ -> Format.ksprintf failwith "%s: write failed" __LOC__
  | 1 -> ()
  | _ -> Format.ksprintf failwith "%s: write failed" __LOC__

let rec select_on_read ~timeout ~fd ~if_timeout ~if_ready =
  match Unix.select [fd] [] [] timeout with
  | exception _ -> Format.ksprintf failwith "%s: select failed (read)" __LOC__
  | ([some_fd], _, _) ->
      assert (some_fd = fd) ;
      if_ready ()
  | _ ->
      if_timeout () ;
      select_on_read ~timeout ~fd ~if_timeout ~if_ready

let rec select_on_write ~timeout ~fd ~if_timeout ~if_ready =
  match Unix.select [] [fd] [] timeout with
  | exception _ -> Format.ksprintf failwith "%s: select failed (write)" __LOC__
  | (_, [some_fd], _) ->
      assert (some_fd = fd) ;
      if_ready ()
  | _ ->
      if_timeout () ;
      select_on_write ~timeout ~fd ~if_timeout ~if_ready

let check_death cleanup ppid =
  if Unix.getppid () <> ppid then (
    Format.printf "shellprobe forkee: parent died, exiting@." ;
    cleanup () ;
    exit 0)
  else ()

(* ------------------------------------------------------------------------- *)
(* Debug target *)

let debug_fmt_and_channel =
  Lazy.from_fun (fun () ->
      let tm = Unix.localtime (Unix.time ()) in
      let suffix =
        Printf.sprintf
          "%d_%d_%d:%d:%d.dat"
          tm.tm_mday
          tm.tm_mon
          tm.tm_hour
          tm.tm_min
          tm.tm_sec
      in
      let file = Filename.temp_file "shell-probe_" suffix in
      let oc =
        open_out_gen
          [Open_wronly; Open_append; Open_creat; Open_trunc; Open_binary]
          0o640
          file
      in
      (Format.formatter_of_out_channel oc, oc))

let debug () = fst (Lazy.force debug_fmt_and_channel)

let close_debug_channel () = close_out (snd (Lazy.force debug_fmt_and_channel))

(* ------------------------------------------------------------------------- *)
(* A chunk contains a bunch of data sent by the writer and the current state
   of the iteration on that data. *)

type chunk =
  { events : buffer;
    metrics : buffer;
    metrics_count : int;
    stride : int;
    iterator : Shell_probe.Tracing.Packet_iterator.state
  }

(* ------------------------------------------------------------------------- *)
(* We perform streaming statistics to adjust the amount of time to allocate
   to packet processing. *)

type stats =
  { mutable writer_period : float;
    mutable copying_time : float;
    mutable event_processing : float;
    mutable iterations : float;
    mutable inv_iterations : float;
    mutable chunks_processed : float;
    mutable chunks_received : float
  }

let create_stats () =
  { writer_period = 0.0;
    copying_time = 0.0;
    (* to avoid infinities cropping up, we assign a minimal time of 1ns to the event processing *)
    event_processing = 1e-9;
    iterations = 0.0;
    inv_iterations = Float.infinity;
    chunks_processed = 0.0;
    chunks_received = 0.0
  }

let update_writer_period stats x =
  let wp = stats.writer_period in
  stats.writer_period <- wp +. ((x -. wp) *. stats.inv_iterations)

let update_copying_time stats x =
  let ct = stats.copying_time in
  stats.copying_time <- ct +. ((x -. ct) *. stats.inv_iterations)

let update_event_processing stats x =
  let ep = stats.event_processing in
  stats.event_processing <- ep +. ((x -. ep) *. stats.inv_iterations)

let increment_iteration stats =
  stats.iterations <- stats.iterations +. 1. ;
  stats.inv_iterations <- 1. /. stats.iterations

let increment_chunks_processed stats =
  stats.chunks_processed <- stats.chunks_processed +. 1.

let increment_chunks_received stats =
  stats.chunks_received <- stats.chunks_received +. 1.

type float_ref = { mutable c : float }

let float_ref c = { c }

let fmax (x : float) (y : float) = if x > y then x else y

let update_avg (avg : float_ref) (x : float) (inv_count : float) =
  avg.c <- avg.c +. ((x -. avg.c) *. inv_count)

(* ------------------------------------------------------------------------- *)
(* main processing loop *)

let print_debug_info fmt stats chunks_queue pool timeouts =
  Format.fprintf
    fmt
    "writer period estimate: %f ms@."
    (stats.writer_period *. 1_000.0) ;
  Format.fprintf
    fmt
    "copying time estimate: %f ms@."
    (stats.copying_time *. 1_000.0) ;
  Format.fprintf
    fmt
    "event processing time estimate: %f musecs@."
    (stats.event_processing *. 1_000_000.0) ;
  Format.fprintf fmt "queue length: %d@." (Queue.length chunks_queue) ;
  Format.fprintf fmt "pool size length: %d@." (Buffer_pool.size pool) ;
  Format.fprintf fmt "chunks processed: %f@." stats.chunks_processed ;
  Format.fprintf fmt "chunks received: %f@." stats.chunks_received ;
  Format.fprintf fmt "timeouts: %d@." timeouts

let start_sliced_processing ~debug_interval ~ppid ~init ~on_event ~on_new_packet
    ~cleanup ~events_buffer ~metrics_buffer ~length_buffer ~write_fd ~read_fd =
  if BA1.dim events_buffer <> BA1.dim metrics_buffer then
    invalid_arg "invalid_arg: start_sliced_processing" ;
  let pipe_buffer = Bytes.create 1 in
  let pool = Buffer_pool.create (BA1.dim events_buffer) in
  let chunks_queue = Queue.create () in
  let current_chunk = ref None in
  let max_timeout = 0.5 in
  (* The algorithm should maintain the following inequality:
     writer_period_estimate >= copying_time_estimate + (time to process events)

     The goal is to ensure that the writer (corresponding to the monitored process)
     waits on us as seldomly as possible, so as not to perturbate its behaviour. *)
  let stats = create_stats () in
  let last_read = float_ref (Unix.gettimeofday ()) in
  let timeouts = ref 0 in
  init () ;
  if debug_interval > 0 then ignore debug_fmt_and_channel ;
  let cleanup () =
    cleanup () ;
    if debug_interval > 0 then close_debug_channel ()
  in
  let rec copying_loop () =
    if
      debug_interval > 0 && int_of_float stats.iterations mod debug_interval = 0
    then
      let fmt = debug () in
      print_debug_info fmt stats chunks_queue pool !timeouts
    else () ;
    (* invariant: processing loop exhausted its quota *)
    (* time to look whether more data is available *)
    let events = Buffer_pool.get pool in
    let metrics = Buffer_pool.get pool in
    select_on_read
      ~timeout:
        stats.writer_period
        (* TODO: the timeout here should be the standard deviation of the estimate,
           _not_ the estimate.
           The current timeout can be problematic if there's a sudden drop in writes:
           we will wait too much on the writer while we could be doing
           some useful work. *)
      ~fd:read_fd
      ~if_timeout:(fun _ ->
        incr timeouts ;
        check_death cleanup ppid)
      ~if_ready:(fun () -> read_byte ~__LOC__ pipe_buffer read_fd) ;
    (* copy data *)
    let t1 = Unix.gettimeofday () in
    BA1.blit events_buffer events ;
    BA1.blit metrics_buffer metrics ;
    let metrics_count = length_buffer.{0} in
    let stride = length_buffer.{1} in
    select_on_write
      ~timeout:max_timeout
      ~fd:write_fd
      ~if_timeout:(fun _ -> check_death cleanup ppid)
      ~if_ready:(fun _ -> write_byte ~__LOC__ pipe_buffer write_fd) ;
    (match Shell_probe.Tracing.Packet_iterator.init ~metrics_count ~stride with
    | None ->
        (* empty chunk: no packets to process *)
        ()
    | Some iterator ->
        increment_chunks_received stats ;
        Queue.add
          { events; metrics; metrics_count; stride; iterator }
          chunks_queue) ;
    (* update estimates *)
    let writer_period = t1 -. last_read.c in
    let t2 = Unix.gettimeofday () in
    let copying_time = t2 -. t1 in
    last_read.c <- t1 ;
    increment_iteration stats ;
    update_writer_period stats writer_period ;
    update_copying_time stats copying_time ;
    let remaining_time = fmax 0.0 (stats.writer_period -. stats.copying_time) in
    processing_loop ~quota:remaining_time
  and processing_loop ~quota =
    match !current_chunk with
    | None -> (
        match Queue.take_opt chunks_queue with
        | None ->
            (* Nothing to do. *)
            copying_loop ()
        | Some chunk as some_chunk ->
            current_chunk := some_chunk ;
            continue_processing ~chunk ~quota)
    | Some chunk -> continue_processing ~chunk ~quota
  and continue_processing ~chunk ~quota =
    let events_quota = int_of_float (quota /. stats.event_processing) in
    let () =
      if
        debug_interval > 0
        && int_of_float stats.iterations mod debug_interval = 0
      then
        let fmt = debug () in
        Format.fprintf
          fmt
          "quota: %f ms, %f events@."
          (quota *. 1_000.0)
          (quota /. stats.event_processing)
      else ()
    in
    if events_quota = 0 then copying_loop ()
    else
      let t1 = Unix.gettimeofday () in
      let events = chunk.events in
      let metrics = chunk.metrics in
      let counter = ref 0 in
      let continue = ref true in
      while !counter < events_quota && !continue do
        incr counter ;
        if
          Shell_probe.Tracing.Packet_iterator.next
            chunk.iterator
            ~events
            ~metrics
            ~on_event
            ~on_new_packet
        then ()
        else (
          (* we exhausted the current chunk, free resources and break *)
          increment_chunks_processed stats ;
          current_chunk := None ;
          Buffer_pool.free pool chunk.events ;
          Buffer_pool.free pool chunk.metrics ;
          continue := false)
      done ;
      let t2 = Unix.gettimeofday () in
      let dt = t2 -. t1 in
      let avg_event_processing_time = dt /. float_of_int !counter in
      update_event_processing stats avg_event_processing_time ;
      let remaining = quota -. dt in
      if (not !continue) && remaining > stats.event_processing then
        (* if we interrupted computation by lack of data, resume
           with the remaining quota *)
        processing_loop ~quota:remaining
      else copying_loop ()
  in
  copying_loop ()

(* ------------------------------------------------------------------------- *)
(* legacy processing, for reference *)

let copy_buffer (b : buffer) =
  let len = BA1.dim b in
  let ba = BA1.create BA.Int BA.c_layout len in
  BA1.blit b ba ;
  ba

let _legacy_start_processing ~ppid ~init ~event_processor ~cleanup
    ~events_buffer ~metrics_buffer ~length_buffer ~write_fd ~read_fd =
  let pipe_buffer = Bytes.create 1 in
  let local_events = copy_buffer events_buffer in
  let local_metrics = copy_buffer metrics_buffer in
  init () ;
  let rec loop () =
    select_on_read
      ~timeout:0.5
      ~fd:read_fd
      ~if_timeout:(fun _ -> check_death cleanup ppid)
      ~if_ready:(fun () -> read_byte ~__LOC__ pipe_buffer read_fd) ;
    BA1.blit events_buffer local_events ;
    BA1.blit metrics_buffer local_metrics ;
    let metrics_count = length_buffer.{0} in
    let stride = length_buffer.{1} in
    select_on_write
      ~timeout:0.5
      ~fd:write_fd
      ~if_timeout:(fun _ -> check_death cleanup ppid)
      ~if_ready:(fun _ -> write_byte ~__LOC__ pipe_buffer write_fd) ;
    let events_count = (metrics_count - 1) * stride in
    event_processor
      ~metrics_count
      ~events_count
      ~stride
      ~events:local_events
      ~metrics:local_metrics ;
    loop ()
  in
  loop ()

let dummy_callback ~events:_ ~metrics_count:_ ~metrics:_ ~stride:_ = ()

(* ------------------------------------------------------------------------- *)
(* entrpoint *)

let start (state : Shell_probe.Tracing.state) ~debug_interval ~init ~on_event
    ~on_new_packet ~cleanup =
  (* we use pipes for signaling data availability
     between the forker and the forkee *)
  let (read_from_forkee, forkee_writes_here) = Unix.pipe ~cloexec:true () in
  let (read_from_forker, forker_writes_here) = Unix.pipe ~cloexec:true () in
  (* mmaping on /dev/zero is not portable to BSDs/macOS. *)
  let fd = Unix.openfile "/dev/zero" [O_RDWR] 0o640 in
  let data =
    Unix.map_file
      fd
      Bigarray.Int
      Bigarray.c_layout
      true
      [| 3; Shell_probe.Tracing.buffer_length state |]
  in
  let data = BA.array2_of_genarray data in
  (* These buffers are where the instrumented process writes its data *)
  let events_buffer = BA2.slice_left data 0 in
  let metrics_buffer = BA2.slice_left data 1 in
  (* We only need the first few elements of [length_buffer] to store
     metric counts but it's easier to setup that way. *)
  let length_buffer = BA2.slice_left data 2 in
  let ppid = Unix.getpid () in
  match Unix.fork () with
  | 0 -> (
      Format.printf
        "shellprobe forkee started (%d), parent = %d, starting processing@."
        (Unix.getpid ())
        ppid ;
      try
        start_sliced_processing
          ~debug_interval
          ~ppid
          ~init
          ~on_event
          ~on_new_packet
          ~cleanup
          ~events_buffer
          ~metrics_buffer
          ~length_buffer
          ~write_fd:forkee_writes_here
          ~read_fd:read_from_forker
      with Failure msg ->
        cleanup () ;
        Format.eprintf "shellprobe forkee: exception caught, exiting (%s)@." msg ;
        exit 1)
  | _id ->
      let buf = Bytes.create 1 in
      let write_callback ~events ~metrics_count ~metrics ~stride =
        BA1.blit events events_buffer ;
        BA1.blit metrics metrics_buffer ;
        length_buffer.{0} <- metrics_count ;
        length_buffer.{1} <- stride ;
        try
          (* This sync. was measured at around 0.3 milliseconds on my laptop.
             Can we do better? *)
          write_byte ~__LOC__ buf forker_writes_here ;
          read_byte ~__LOC__ buf read_from_forkee
        with _ ->
          (* Error caught; the forkee is probably dead. *)
          Format.eprintf "shellprobe: error detected, trace recording stopped@." ;
          Shell_probe.Tracing.set_snapshot_callback state dummy_callback
      in
      Shell_probe.Tracing.set_snapshot_callback state write_callback
